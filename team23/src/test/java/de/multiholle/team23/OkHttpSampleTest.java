package de.multiholle.team23;

import com.google.gson.Gson;

import org.junit.Test;

import java.io.IOException;
import java.util.Map;

import me.figo.FigoConnection;
import me.figo.FigoException;
import me.figo.FigoPinException;
import me.figo.FigoSession;
import me.figo.internal.FigoTrustManager;
import me.figo.internal.TaskStatusResponse;
import me.figo.internal.TaskTokenResponse;
import me.figo.internal.TokenResponse;
import me.figo.models.Account;
import me.figo.models.Bank;
import me.figo.models.Category;
import me.figo.models.LoginSettings;
import me.figo.models.Transaction;
import me.figo.models.User;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class OkHttpSampleTest {
    private final OkHttpClient client = new OkHttpClient();
    private final Gson gson = new Gson();

    private static final String CLIENT_ID = "Cv3blO3LmreaTwOKh_25PNkFESMp_SnIWVbPVk677aMs";
    private static final String CLIENT_SECRET = "SYvY6bAvtYDQgU6RZelM7YsX9iRkvUV0c_Q0vtqd1cYI";
    private static final String API_ENDPOINT = "https://bankathon.figo.me";
    private static final int TIMEOUT = 10000;
    private static final String USERNAME = "figo";
    private static final String PASSWORT = "figo";
    private static final String BANK_CODE = "90090099";
    private static final String COUNTRY_CODE = "de";

    @Test
    public void addition_isCorrect() throws Exception {
        Request request = new Request.Builder()
                .url("https://api.github.com/gists/c2a7c39532239ff261be")
                .build();
        Response response = client.newCall(request).execute();
        if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);

        Gist gist = gson.fromJson(response.body().charStream(), Gist.class);
        for (Map.Entry<String, GistFile> entry : gist.files.entrySet()) {
            System.out.println(entry.getKey());
            System.out.println(entry.getValue().content);
        }

    }

    @Test
    public void test() throws FigoException, FigoPinException, IOException, InterruptedException {
        FigoTrustManager trustManager = new FigoTrustManager();
        trustManager.addTrustedFingerprint("DA09004300254B651922952BCE6927842C3F9810");
        trustManager.addTrustedFingerprint("DBE2E9158FC9903084FE36CAA61138D85A205D93");

        FigoConnection connection = new FigoConnection(CLIENT_ID, CLIENT_SECRET, "127.0.0.1", TIMEOUT, API_ENDPOINT);
        connection.setTrustManager(trustManager);
        // connection.addUser("thomas", "thomas@comdirect.de", "thomas", "DE");

        TokenResponse tokenResponse = connection.credentialLogin("thomas@comdirect.de", "thomas");

        String accessToken = tokenResponse.getAccessToken();
        FigoSession session = new FigoSession(accessToken, TIMEOUT, API_ENDPOINT);
        session.setTrustManager(trustManager);


//        TaskTokenResponse taskTokenResponse = session.setupNewAccount(BANK_CODE, COUNTRY_CODE, USERNAME, PASSWORT, null, true, false);
//        TaskStatusResponse taskStatus = session.getTaskState(taskTokenResponse);
//        while (!taskStatus.isEnded() && !taskStatus.isErroneous() && !taskStatus.isWaitingForPin() && !taskStatus.isWaitingForResponse()) {
//            taskStatus = session.getTaskState(taskTokenResponse);
//            Thread.sleep(1000);
//        }
//        if (taskStatus.isWaitingForPin() && !taskStatus.isEnded()) {
//            throw new FigoPinException(BANK_CODE, COUNTRY_CODE, USERNAME, PASSWORT, taskTokenResponse);
//        } else if (taskStatus.isErroneous() && taskStatus.isEnded()) {
//            throw new FigoException("", taskStatus.getMessage());
//        }


        // print out a list of accounts including its balance
        for (Account account : session.getAccounts()) {
            System.out.println(account.getName());
            System.out.println(session.getAccountBalance(account).getBalance());

//                System.out.println("Name of originator or recipient: " + transaction.getName());
//                System.out.println("Amount: " + transaction.getAmount());
//                System.out.println("Currency: " + transaction.getCurrency());
//                System.out.println("Account number: " + transaction.getAccountNumber());
//                System.out.println("Bank code: " + transaction.getBankCode());
//                System.out.println("Bank name: " + transaction.getBankName());
//                System.out.println("Booking date: " + transaction.getBookingDate());
//                System.out.println("Value date: " + transaction.getValueDate());
//                System.out.println("Purpose text: " + transaction.getPurposeText());
//                System.out.println("Type: " + transaction.getType());
//                System.out.println("Booking text: " + transaction.getBookingText());
//                System.out.println("Booked: " + (transaction.isBooked() ? "yes" : "no"));
//                System.out.println("Visited: " + (transaction.isVisited() ? "yes" : "no"));


            StringBuilder sb = new StringBuilder();
            sb.append("Name of originator or recipient;");
            sb.append("Amount;");
            sb.append("Currency;");
            sb.append("Account number;");
            sb.append("Bank code;");
            sb.append("Bank name;");
            sb.append("Booking date;");
            sb.append("Value date;");
            sb.append("Purpose text;");
            sb.append("Type;");
            sb.append("Booking text;");
            sb.append("Booked;");
            sb.append("Visited;");
            sb.append("Categories;");
            System.out.println(sb.toString());

            for (Transaction transaction : session.getTransactions(account)) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append(transaction.getName() + ";");
                sb2.append(transaction.getAmount() + ";");
                sb2.append(transaction.getCurrency() + ";");
                sb2.append(transaction.getAccountNumber() + ";");
                sb2.append(transaction.getBankCode() + ";");
                sb2.append(transaction.getBankName() + ";");
                sb2.append(transaction.getBookingDate() + ";");
                sb2.append(transaction.getValueDate() + ";");
                sb2.append(transaction.getPurposeText() + ";");
                sb2.append(transaction.getType() + ";");
                sb2.append(transaction.getBookingText() + ";");
                sb2.append((transaction.isBooked() ? "yes" : "no") + ";");
                sb2.append((transaction.isVisited() ? "yes" : "no") + ";");

                for (Category category : transaction.getCategories()) {
                    sb2.append(category.getName() + ",");
                }
                sb2.append(";");
                System.out.println(sb2.toString());
            }
        }
    }

    static class Gist {
        Map<String, GistFile> files;
    }

    static class GistFile {
        String content;
    }
}