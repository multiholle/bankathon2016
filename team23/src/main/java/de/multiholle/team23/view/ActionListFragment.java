package de.multiholle.team23.view;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.widget.Button;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import de.multiholle.team23.R;
import de.multiholle.team23.compontents.WaveHelper;
import de.multiholle.team23.compontents.WaveView;
import de.multiholle.team23.service.action.Action;
import de.multiholle.team23.service.action.ActionService;
import de.multiholle.team23.util.CategoryMappingUtils;
import de.multiholle.team23.util.NotificationUtils;

public class ActionListFragment extends Fragment {
    private DecimalFormat formatter = new DecimalFormat("#");

    @BindView(R.id.sum)
    TextView sum;

    @BindView(R.id.goal)
    TextView goal;

    @BindView(R.id.wave)
    WaveView waveView;

    @BindView(R.id.list)
    RecyclerView recyclerView;

    @BindView(R.id.drop)
    View drop;

    private RecyclerView.LayoutManager layoutManager;
    private ActionListAdapter adapter;
    private WaveHelper waveHelper;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_action_list, container, false);

        ButterKnife.bind(this, view);


        layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);

        adapter = new ActionListAdapter();
        recyclerView.setAdapter(adapter);

        recyclerView.addItemDecoration(new SimpleDividerItemDecoration(getContext()));

        waveHelper = new WaveHelper(waveView);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        adapter.setActions(ActionService.getInstance().fetchActions());

        updateHeader();

        waveHelper.start();
    }

    @Override
    public void onPause() {
        super.onPause();

        waveHelper.cancel();
    }

    private void updateHeader() {
        sum.setText(formatter.format(ActionService.getInstance().fetchSum()) + " €");
        goal.setText("Goal: " + formatter.format(ActionService.getInstance().fetchGoal()) + " €");

        waveView.setWaterLevelRatio(ActionService.getInstance().fetchRatio());
    }

    public class ActionListAdapter extends RecyclerView.Adapter<ActionListAdapter.ViewHolder> {
        private List<Action> actions;
        int expandedPosition = -1;

        public class ViewHolder extends RecyclerView.ViewHolder {
            @BindView(R.id.logo)
            CircleImageView imageView;

            @BindView(R.id.description)
            TextView description;

            @BindView(R.id.savedAmount)
            TextView savedAmount;

            @BindView(R.id.category)
            TextView category;

            @BindView(R.id.more_name)
            TextView moreName;

            @BindView(R.id.more_desc)
            TextView moreDesc;

            @BindView(R.id.details_container)
            View detailsContainer;

            @BindView(R.id.yolo)
            Button yolo;

            public ViewHolder(View view) {
                super(view);

                ButterKnife.bind(this, view);
            }
        }

        public void setActions(List<Action> actions) {
            this.actions = actions;
            notifyDataSetChanged();
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            // create a new view
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_action, parent, false);

            ViewHolder viewHolder = new ViewHolder(v);
            return viewHolder;
        }


        // Replace the contents of a view (invoked by the layout manager)
        @Override
        public void onBindViewHolder(ViewHolder holder, final int position) {
            final Action action = actions.get(position);
            holder.description.setText(action.name);
            holder.savedAmount.setText(formatter.format(action.savedAmount) + " €");
            holder.category.setBackground(getContext().getDrawable(CategoryMappingUtils.toBackground(action.category)));
            holder.category.setText(CategoryMappingUtils.toDescription(action.category));
            holder.moreName.setText(action.name);
            holder.moreDesc.setText(action.more + " " + action.amount + " €");

            int id = getContext().getResources().getIdentifier(action.iconUrl, "drawable", getContext().getPackageName());
            holder.imageView.setImageResource(id);

            final boolean isExpanded = position == expandedPosition;
            holder.detailsContainer.setVisibility(isExpanded ? View.VISIBLE : View.GONE);
            holder.itemView.setActivated(isExpanded);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    expandedPosition = isExpanded ? -1 : position;
                    //TransitionManager.beginDelayedTransition(recyclerView);
                    notifyDataSetChanged();
                }
            });

            holder.yolo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    action.savedAmount = 2 * action.savedAmount;
                    ActionService.getInstance().updateActionSavedAmount(action.id, action.savedAmount);
                    notifyDataSetChanged();
                    updateHeader();

                    ObjectAnimator animator = ObjectAnimator.ofFloat(drop, "translationY", 540);
                    animator.setDuration(700);
                    animator.setInterpolator(new AccelerateInterpolator(2.0f));
                    animator.addListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {

                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {
                            drop.setTranslationY(0);
                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {

                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {

                        }
                    });
                    animator.start();


                }
            });
        }

        // Return the size of your dataset (invoked by the layout manager)
        @Override
        public int getItemCount() {
            return actions.size();
        }
    }
}
