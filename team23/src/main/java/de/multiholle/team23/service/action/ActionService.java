package de.multiholle.team23.service.action;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import de.multiholle.team23.Team23Application;

/**
 * Created by marcelholle on 10/10/16.
 */

public class ActionService {
    private static final double GOAL = 3000.0;

    private static final ActionService instance = new ActionService();

    private Gson gson = new Gson();

    private ActionService() {
        sharedPreferences().edit().clear().apply();
    }

    public static ActionService getInstance() {
        return instance;
    }

    public double fetchSum() {
        double sum = 0;
        for (Action action : fetchActions()) {
            sum += action.savedAmount;
        }

        return sum + 600;
    }

    public double fetchGoal() {
        return GOAL;
    }

    public float fetchRatio() {
        return (float) (fetchSum() / fetchGoal());
    }

    public List<Action> fetchActions() {
        if (!sharedPreferences().contains("Action")) {
            storeActions(getDefaultActions());
        }

        String json = sharedPreferences().getString("Action", null);
        if (json == null) {
            return new ArrayList<>();
        }

        return gson.fromJson(json, new TypeToken<List<Action>>() {}.getType());
    }

    public void storeActions(List<Action> actions) {
        sharedPreferences()
                .edit()
                .putString("Action", gson.toJson(actions))
                .apply();
    }

    public void updateActionSavedAmount(int id, double savedAmount) {
        List<Action> actions = fetchActions();
        for (Action action : actions) {
            if (action.id == id) {
                action.savedAmount = savedAmount;
            }
        }

        storeActions(actions);
    }

    public void confirmPendingAction() {
        if (!sharedPreferences().contains("PendingAction")) {
            return;
        }

        Action action = gson.fromJson(sharedPreferences().getString("PendingAction", null), Action.class);

        LinkedList<Action> actions = new LinkedList<>(fetchActions());
        actions.addFirst(action);
        storeActions(actions);
    }

    public void storePendingAction(Action action) {
        sharedPreferences()
                .edit()
                .putString("PendingAction", gson.toJson(action))
                .apply();
    }

    public void clearPendingAction(Action action) {
        sharedPreferences()
                .edit()
                .remove("PendingAction")
                .apply();
    }

    private SharedPreferences sharedPreferences() {
        return Team23Application.getContext().getSharedPreferences("Action", Context.MODE_PRIVATE);
    }

    private List<Action> getDefaultActions() {
        List<Action> actions = new LinkedList<>();
        actions.add(new Action(0,"saturn","Saturn Sagt Danke 65423106",-71.69,7.16,5,"Hamburger Sparkasse","11.10.2016 16:45","Ec-Karte"));
        actions.add(new Action(1,"amazon","Amazon Eu S.A R.L., Niederlassung Deutschland",-130.5,13.05,5,"Hamburger Sparkasse","09.10.2016 12:23","Lastschrift"));
        actions.add(new Action(2,"douglas","Parfumerie Douglas Gmbh",-69.95,6.99,4,"comdirect","09.10.2016 11:57","Ec-Karte"));
        actions.add(new Action(3,"hardrock","Hard Rock Cafe Hamburg Hade",-93.3,9.33,1,"Hamburger Sparkasse","07.10.2016 10:35","Ec-Karte"));
        actions.add(new Action(4,"stpauli","Fc St. Pauli Von 1910 E.V.",-60.00,6.00,2,"comdirect","06.10.2016 16:24","Lastschrift"));
        actions.add(new Action(5,"wormland","Wormland Hamburg",-69.98,6.99,5,"comdirect","04.10.2016 13:54","Ec-Karte"));
        actions.add(new Action(6,"cinemaxx","CinemaxX Hamburg",-31.40,3.14,0,"Hamburger Sparkasse","01.10.2016 17:31","Ec-Karte"));
        actions.add(new Action(7,"hhotel","25h Hotel Berlin",-254.50,25.45,3,"comdirect","30.09.2016 09:47","Lastschrift"));
        actions.add(new Action(8,"hollister","Hollister Co Europa Passage",-78.00,7.8,5,"Hamburger Sparkasse","27.09.2016 08:55","Ec-Karte"));
        actions.add(new Action(9,"pieper","Parfumerie Pieper Gmbh",-108.00,10.8,4,"Hamburger Sparkasse","26.09.2016 16:16","Ec-Karte"));
        actions.add(new Action(10,"sportscheck","Sport Scheck Hamburg",-49.9,4.99,5,"comdirect","25.09.2016 14:12","Ec-Karte"));
        actions.add(new Action(11,"muenchnerhof","Hotel Münchner Hof",-197.00,19.7,3,"Hamburger Sparkasse","22.09.2016 12:11","Lastschrift"));
        actions.add(new Action(12,"kofookoo","kofookoo Sushi Hamburg",-82.00,8.2,1,"Hamburger Sparkasse","21.09.2016 11:41","Ec-Karte"));


        return actions;
    }
}
