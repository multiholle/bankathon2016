package de.multiholle.team23.service.gcm;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import de.multiholle.team23.Team23Application;
import de.multiholle.team23.service.action.ActionService;
import de.multiholle.team23.view.MainActivity;

/**
 * Created by marcelholle on 11/10/16.
 */

public class SaveMoneyReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("ASDF", intent.getAction());

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(42);

        if (!intent.getAction().equals("de.multiholle.team23.CANCEL")) {
            ActionService.getInstance().confirmPendingAction();

            Intent i = new Intent(context, MainActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(i);

            context.sendBroadcast(new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS));
        }
    }
}
