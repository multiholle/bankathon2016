package de.multiholle.team23.service.action;

import android.support.annotation.NonNull;

import com.google.gson.Gson;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import me.figo.FigoConnection;
import me.figo.FigoException;
import me.figo.FigoPinException;
import me.figo.FigoSession;
import me.figo.internal.FigoTrustManager;
import me.figo.internal.TokenResponse;
import me.figo.models.Account;
import me.figo.models.Transaction;
import okhttp3.OkHttpClient;

/**
 * Created by A4722401 on 11.10.2016.
 */

public class FigoService {
    private final OkHttpClient client = new OkHttpClient();
    private final Gson gson = new Gson();

    private static final String CLIENT_ID = "Cv3blO3LmreaTwOKh_25PNkFESMp_SnIWVbPVk677aMs";
    private static final String CLIENT_SECRET = "SYvY6bAvtYDQgU6RZelM7YsX9iRkvUV0c_Q0vtqd1cYI";
    private static final String API_ENDPOINT = "https://bankathon.figo.me";
    private static final int TIMEOUT = 10000;
    private static final String USERNAME = "figo";
    private static final String PASSWORT = "figo";
    private static final String BANK_CODE = "90090099";
    private static final String COUNTRY_CODE = "de";


    public List<Account> getAccounts() throws FigoException, FigoPinException, IOException, InterruptedException
    {
        FigoSession session = getFigoSession();
        return session.getAccounts();
    }

    public List<Transaction> getTransactions() throws FigoException, FigoPinException, IOException, InterruptedException
    {
        List<Transaction> transactions = new ArrayList<>();
        FigoSession session = getFigoSession();

        for (Account account : getAccounts()) {
            transactions.addAll(session.getTransactions(account));
        }

        return transactions;
    }

    @NonNull
    private FigoSession getFigoSession() throws IOException, FigoException {
        FigoTrustManager trustManager = new FigoTrustManager();
        trustManager.addTrustedFingerprint("DA09004300254B651922952BCE6927842C3F9810");
        trustManager.addTrustedFingerprint("DBE2E9158FC9903084FE36CAA61138D85A205D93");

        FigoConnection connection = new FigoConnection(CLIENT_ID, CLIENT_SECRET, "127.0.0.1", TIMEOUT, API_ENDPOINT);
        connection.setTrustManager(trustManager);
        // connection.addUser("thomas", "thomas@comdirect.de", "thomas", "DE");

        TokenResponse tokenResponse = connection.credentialLogin("thomas@comdirect.de", "thomas");

        String accessToken = tokenResponse.getAccessToken();
        FigoSession session = new FigoSession(accessToken, TIMEOUT, API_ENDPOINT);
        session.setTrustManager(trustManager);


//        TaskTokenResponse taskTokenResponse = session.setupNewAccount(BANK_CODE, COUNTRY_CODE, USERNAME, PASSWORT, null, true, false);
//        TaskStatusResponse taskStatus = session.getTaskState(taskTokenResponse);
//        while (!taskStatus.isEnded() && !taskStatus.isErroneous() && !taskStatus.isWaitingForPin() && !taskStatus.isWaitingForResponse()) {
//            taskStatus = session.getTaskState(taskTokenResponse);
//            Thread.sleep(1000);
//        }
//        if (taskStatus.isWaitingForPin() && !taskStatus.isEnded()) {
//            throw new FigoPinException(BANK_CODE, COUNTRY_CODE, USERNAME, PASSWORT, taskTokenResponse);
//        } else if (taskStatus.isErroneous() && taskStatus.isEnded()) {
//            throw new FigoException("", taskStatus.getMessage());
//        }
        return session;
    }
}
