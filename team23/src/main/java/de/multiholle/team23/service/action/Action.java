package de.multiholle.team23.service.action;


import org.apache.commons.lang3.StringUtils;

/**
 * Created by marcelholle on 10/10/16.
 */

public class Action {
    public int id;
    public String iconUrl;
    public String name;
    public double amount;
    public double savedAmount;
    public int category;
    public String more;

    public Action(int id, String iconUrl, String name, Double amount, Double savedAmount, int category, String... more) {
        this.id = id;
        this.iconUrl = iconUrl;
        this.name = name;
        this.amount = amount;
        this.savedAmount = savedAmount;
        this.category = category;
        this.more = StringUtils.join(more, '\n');
    }
}
