package de.multiholle.team23.service.gcm;

import android.os.Bundle;

import com.google.android.gms.gcm.GcmListenerService;
import com.google.gson.Gson;

import java.text.DecimalFormat;
import java.util.Set;

import de.multiholle.team23.Team23Application;
import de.multiholle.team23.service.action.Action;
import de.multiholle.team23.service.action.ActionService;
import de.multiholle.team23.util.NotificationUtils;

public class NotificationsListenerService extends GcmListenerService {
    private DecimalFormat formatterShort = new DecimalFormat("#");
    private DecimalFormat formatterLong = new DecimalFormat("#.##");

    @Override
    public void onMessageReceived(String s, Bundle bundle) {
        String body = bundle.getString("body");
        Gson gson = new Gson();
        Action action = gson.fromJson(body, Action.class);

        ActionService.getInstance().storePendingAction(action);

        //String json = gson.toJson(new Action(42, "asdf.jpg", "Rewe sagt danke!", 213.21, 21.0, 0, "more"));
        // {"amount":213.21,"category":0,"iconUrl":"asdf.jpg","id":42,"more":"more","name":"Rewe sagt danke!","savedAmount":21.0}


        NotificationUtils.notify(formatterLong.format(action.amount) + " €", action.name, formatterShort.format(action.savedAmount) + " €", Team23Application.getContext());
    }
}