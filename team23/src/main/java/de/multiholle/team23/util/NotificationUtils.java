package de.multiholle.team23.util;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;

import de.multiholle.team23.view.MainActivity;
import de.multiholle.team23.R;

/**
 * Created by marcelholle on 10/10/16.
 */

public class NotificationUtils {
    public static void notify(String amount, String transaction, String savingsAmount, Context context) {
        String title = "Drop a small invest";
        String message = "You just spent " + amount + " for \"" + transaction + "\". Reach your big dreams with Trickle. Invest " + savingsAmount + " now!";
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
                    .setSmallIcon(R.drawable.logo_white)
                    .setContentTitle(title)
                    .setContentText(message);

        // Creates an explicit intent for an Activity in your app
        Intent resultIntent = new Intent(context, MainActivity.class);

        // The stack builder object will contain an artificial back stack for the
        // started Activity.
        // This ensures that navigating backward from the Activity leads out of
        // your application to the Home screen.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);

        // Adds the back stack for the Intent (but not the Intent itself)
        stackBuilder.addParentStack(MainActivity.class);

        // Adds the Intent that starts the Activity to the top of the stack
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(
                    0,
                    PendingIntent.FLAG_UPDATE_CURRENT
        );
        mBuilder.setContentIntent(resultPendingIntent);

        PendingIntent saveMoneyPendingIntent = PendingIntent.getBroadcast(context, 0, new Intent("de.multiholle.team23.SEND_MONEY"), 0);
        PendingIntent cancelPendingIntent = PendingIntent.getBroadcast(context, 0, new Intent("de.multiholle.team23.CANCEL"), 0);


        mBuilder.addAction(R.drawable.ic_done_black_24dp, "Trickle now", saveMoneyPendingIntent);
        mBuilder.addAction(R.drawable.ic_close_black_24dp, "Later", cancelPendingIntent);


        mBuilder.setStyle(new NotificationCompat.BigTextStyle()
                .bigText(message));

        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        mBuilder.setSound(alarmSound);

        NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        // mId allows you to update the notification later on.
        mNotificationManager.notify(42, mBuilder.build());
    }
}
