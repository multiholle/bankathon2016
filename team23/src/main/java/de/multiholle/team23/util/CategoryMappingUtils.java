package de.multiholle.team23.util;

import de.multiholle.team23.R;

/**
 * Created by marcelholle on 10/10/16.
 */

public class CategoryMappingUtils {
    public static String toDescription(int category) {
        switch (category) {
            case 0:
                return "Concerts/Cinema";
            case 1:
                return "Restaurant";
            case 2:
                return "Sports events";
            case 3:
                return "Travel";
            case 4:
                return "Cosmetics";
            case 5:
                return "Shopping";
            default:
                return null;
        }
    }

    public static int toBackground(int category) {
        switch (category) {
            case 0:
                return R.drawable.button_background_c1_s;
            case 1:
                return R.drawable.button_background_c2_s;
            case 2:
                return R.drawable.button_background_c3_s;
            case 3:
                return R.drawable.button_background_c4_s;
            case 4:
                return R.drawable.button_background_c5_s;
            case 5:
                return R.drawable.button_background_c6_s;
            default:
                return 0;
        }
    }
}
