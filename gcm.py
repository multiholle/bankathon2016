from urllib2 import *
import urllib
import json
import sys

MY_API_KEY="AIzaSyAaOzjcUB2cWp_Xpk4D9r2Nni6oyuvsJqQ"

messageTitle = sys.argv[1]
messageBody = sys.argv[2]

data={
    "to" : "/topics/spending",
    "data" : {
        "body" : messageBody,
        "title" : messageTitle,
        "icon" : "ic_attach_money_white_48dp"
    }
}

dataAsJSON = json.dumps(data)

request = Request(
    "https://gcm-http.googleapis.com/gcm/send",
    dataAsJSON,
    { "Authorization" : "key="+MY_API_KEY,
      "Content-type" : "application/json"
    }
)

print urlopen(request).read()


# {"amount":213.21,"category":0,"iconUrl":"asdf.jpg","id":42,"more":"more","name":"Rewe sagt danke!","savedAmount":21.0}