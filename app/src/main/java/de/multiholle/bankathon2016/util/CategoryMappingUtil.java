package de.multiholle.bankathon2016.util;

/**
 * Created by marcelholle on 10/10/16.
 */

public class CategoryMappingUtil {
    public static String toDescription(int category) {
        switch (category) {
            case 0:
                return "Concerts/Cinema";
            case 1:
                return "Restaurant";
            case 2:
                return "Sports events";
            case 3:
                return "Travel";
            case 4:
                return "Cosmetics";
            case 5:
                return "Shopping";
            default:
                return null;
        }
    }
}
