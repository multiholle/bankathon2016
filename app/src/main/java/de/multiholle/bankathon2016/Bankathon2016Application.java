package de.multiholle.bankathon2016;

import android.app.Application;
import android.content.Context;
import android.content.Intent;

import de.multiholle.bankathon2016.service.gcm.RegistrationService;

/**
 * Created by marcelholle on 10/10/16.
 */

public class Bankathon2016Application extends Application {
    private static Context applicationContext;

    public static Context getContext() {
        return applicationContext;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        applicationContext = getApplicationContext();

        startService(new Intent(this, RegistrationService.class));
    }
}
