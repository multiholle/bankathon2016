package de.multiholle.bankathon2016.view.link_bank_account;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import de.multiholle.bankathon2016.R;
import de.multiholle.bankathon2016.view.link_credit_card.LinkCreditCardActivity;

public class LinkBankAccountActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_link_bank_account);

        setTitle("Link Bank Account");
    }

    public void onNextButtonClick(View view) {
        startActivity(new Intent(this, LinkCreditCardActivity.class));
    }
}
