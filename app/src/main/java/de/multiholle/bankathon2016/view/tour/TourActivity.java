package de.multiholle.bankathon2016.view.tour;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.multiholle.bankathon2016.R;
import de.multiholle.bankathon2016.view.link_bank_account.LinkBankAccountActivity;

public class TourActivity extends FragmentActivity {



    @BindView(R.id.view_pager)
    ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_tour);
        ButterKnife.bind(this);

        viewPager.setAdapter(new TourPagerAdapter(getSupportFragmentManager()));
    }

    @Override
    public void onBackPressed() {
        if (viewPager.getCurrentItem() == 0) {
            super.onBackPressed();
        } else {
            viewPager.setCurrentItem(viewPager.getCurrentItem() - 1);
        }
    }

    public void onButtonGoClick(View view) {
        startActivity(new Intent(this, LinkBankAccountActivity.class));
    }

    private class TourPagerAdapter extends FragmentStatePagerAdapter {
        public TourPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new TourSlide1Fragment();
                case 1:
                    return new TourSlide2Fragment();
                case 2:
                    return new TourSlide3Fragment();
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return 3;
        }
    }
}
