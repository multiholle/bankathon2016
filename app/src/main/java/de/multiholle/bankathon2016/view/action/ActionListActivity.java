package de.multiholle.bankathon2016.view.action;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.text.DecimalFormat;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import de.multiholle.bankathon2016.R;
import de.multiholle.bankathon2016.service.action.Action;
import de.multiholle.bankathon2016.service.action.ActionService;
import de.multiholle.bankathon2016.util.CategoryMappingUtil;

public class ActionListActivity extends Activity {

    @BindView(R.id.list)
    ListView listView;

    private ActionListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_action_list);
        ButterKnife.bind(this);

        adapter = new ActionListAdapter(this);
        listView.setAdapter(adapter);
    }

    @Override
    protected void onResume() {
        super.onResume();

        adapter.clear();
        adapter.addAll(ActionService.getInstance().fetchActions());

        adapter.add(new Action(null, "Test Element", 12.0, 0));
        adapter.add(new Action(null, "Test Element", 12.0, 1));
        adapter.add(new Action(null, "Test Element", 12.0, 4));
        adapter.add(new Action(null, "Test Element", 12.0, 5));
        adapter.add(new Action(null, "Test Element", 12.0, 1));
        adapter.add(new Action(null, "Test Element", 12.0, 2));
        adapter.add(new Action(null, "Test Element", 12.0, 2));
        adapter.add(new Action(null, "Test Element", 12.0, 1));
        adapter.add(new Action(null, "Test Element", 12.0, 3));
        adapter.add(new Action(null, "Test Element", 12.0, 0));
        adapter.add(new Action(null, "Test Element", 12.0, 1));
        adapter.add(new Action(null, "Test Element", 12.0, 4));
        adapter.add(new Action(null, "Test Element", 12.0, 5));
        adapter.add(new Action(null, "Test Element", 12.0, 1));
        adapter.add(new Action(null, "Test Element", 12.0, 2));
        adapter.add(new Action(null, "Test Element", 12.0, 2));
        adapter.add(new Action(null, "Test Element", 12.0, 1));
        adapter.add(new Action(null, "Test Element", 12.0, 3));
    }

    public static class ActionListAdapter extends ArrayAdapter<Action> {
        private LayoutInflater inflater;

        public ActionListAdapter(Context context) {
            super(context, 0);

            inflater = LayoutInflater.from(context);
        }

        @NonNull
        @Override
        public View getView(int position, View view, ViewGroup parent) {
            ViewHolder holder;
            if (view == null) {
                view = inflater.inflate(R.layout.view_transaction, parent, false);
                holder = new ViewHolder(view);
                view.setTag(holder);
            } else {
                holder = (ViewHolder) view.getTag();
            }

            Action action = getItem(position);
            holder.description.setText(action.name);

            DecimalFormat formatter = new DecimalFormat("#");
            holder.amount.setText(formatter.format(action.amount) + "€");

            switch (action.category) {
                case 0:
                    holder.category.setBackground(getContext().getDrawable(R.drawable.button_background_c1_s));
                    break;
                case 1:
                    holder.category.setBackground(getContext().getDrawable(R.drawable.button_background_c2_s));
                    break;
                case 2:
                    holder.category.setBackground(getContext().getDrawable(R.drawable.button_background_c3_s));
                    break;
                case 3:
                    holder.category.setBackground(getContext().getDrawable(R.drawable.button_background_c4_s));
                    break;
                case 4:
                    holder.category.setBackground(getContext().getDrawable(R.drawable.button_background_c5_s));
                    break;
                case 5:
                    holder.category.setBackground(getContext().getDrawable(R.drawable.button_background_c6_s));
                    break;
            }
            holder.category.setText(CategoryMappingUtil.toDescription(action.category));

            return view;
        }

        static class ViewHolder {
            @BindView(R.id.logo)
            CircleImageView imageView;

            @BindView(R.id.description)
            TextView description;

            @BindView(R.id.amount)
            TextView amount;

            @BindView(R.id.category)
            TextView category;

            public ViewHolder(View view) {
                ButterKnife.bind(this, view);
            }
        }
    }
}
