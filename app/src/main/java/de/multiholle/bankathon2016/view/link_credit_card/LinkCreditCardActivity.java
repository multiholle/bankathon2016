package de.multiholle.bankathon2016.view.link_credit_card;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import de.multiholle.bankathon2016.R;
import de.multiholle.bankathon2016.view.action.ActionListActivity;

public class LinkCreditCardActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_link_credit_card);

        setTitle("Link Credit Card");
    }

    public void onNextButtonClick(View view) {
        startActivity(new Intent(this, ActionListActivity.class));
    }
}
