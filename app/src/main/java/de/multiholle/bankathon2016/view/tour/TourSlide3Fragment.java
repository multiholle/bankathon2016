package de.multiholle.bankathon2016.view.tour;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.multiholle.bankathon2016.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class TourSlide3Fragment extends Fragment {

    private boolean c1 = false;
    private boolean c2 = false;
    private boolean c3 = false;
    private boolean c4 = false;
    private boolean c5 = false;
    private boolean c6 = false;

    @BindView(R.id.c1)
    View c1View;

    @BindView(R.id.c2)
    View c2View;

    @BindView(R.id.c3)
    View c3View;

    @BindView(R.id.c4)
    View c4View;

    @BindView(R.id.c5)
    View c5View;

    @BindView(R.id.c6)
    View c6View;

    public TourSlide3Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_tour_slide3, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        updateC();
    }

    @OnClick(R.id.c1)
    public void onC1Click(View view) {
        c1 = !c1;
        updateC();
    }

    @OnClick(R.id.c2)
    public void onC2Click(View view) {
        c2 = !c2;
        updateC();
    }

    @OnClick(R.id.c3)
    public void onC3Click(View view) {
        c3 = !c3;
        updateC();
    }

    @OnClick(R.id.c4)
    public void onC4Click(View view) {
        c4 = !c4;
        updateC();
    }

    @OnClick(R.id.c5)
    public void onC5Click(View view) {
        c5 = !c5;
        updateC();
    }

    @OnClick(R.id.c6)
    public void onC6Click(View view) {
        c6 = !c6;
        updateC();
    }

    private void updateC() {
        c1View.setBackground(getContext().getDrawable(c1 ? R.drawable.button_background_c1 : R.drawable.button_background_cx));
        c2View.setBackground(getContext().getDrawable(c2 ? R.drawable.button_background_c2 : R.drawable.button_background_cx));
        c3View.setBackground(getContext().getDrawable(c3 ? R.drawable.button_background_c3 : R.drawable.button_background_cx));
        c4View.setBackground(getContext().getDrawable(c4 ? R.drawable.button_background_c4 : R.drawable.button_background_cx));
        c5View.setBackground(getContext().getDrawable(c5 ? R.drawable.button_background_c5 : R.drawable.button_background_cx));
        c6View.setBackground(getContext().getDrawable(c6 ? R.drawable.button_background_c6 : R.drawable.button_background_cx));
    }
}
