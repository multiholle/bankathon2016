package de.multiholle.bankathon2016.service.action;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

import de.multiholle.bankathon2016.Bankathon2016Application;

/**
 * Created by marcelholle on 10/10/16.
 */

public class ActionService {
    private static final ActionService instance = new ActionService();

    private Gson gson = new Gson();

    private ActionService() {}

    public static ActionService getInstance() {
        return instance;
    }

    public List<Action> fetchActions() {
        String json = sharedPreferences().getString("Action", null);
        if (json == null) {
            return new ArrayList<>();
        }

        return gson.fromJson(json, new TypeToken<List<Action>>(){}.getType());
    }

    public void storeActions(List<Action> actions) {
        sharedPreferences()
                .edit()
                .putString("Action", gson.toJson(actions))
                .apply();
    }

    private SharedPreferences sharedPreferences() {
        return Bankathon2016Application.getContext().getSharedPreferences("Action", Context.MODE_PRIVATE);
    }
}
