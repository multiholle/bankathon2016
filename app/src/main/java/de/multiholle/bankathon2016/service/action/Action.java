package de.multiholle.bankathon2016.service.action;

/**
 * Created by marcelholle on 10/10/16.
 */

public class Action {
    public String iconUrl;
    public String name;
    public Double amount;
    public int category;

    public Action(String iconUrl, String name, Double amount, int category) {
        this.iconUrl = iconUrl;
        this.name = name;
        this.amount = amount;
        this.category = category;
    }
}
